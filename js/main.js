//Variables globales
var divisionSelectedValue='LigaBBVA'; //--Por defecto usamos la Liga BBVA

//--Botones de la parte visual
function drawSelectedGraph(){
    eval($("#graphSelector").val());
}

function updateDivisionSelected() {
    divisionSelectedValue=$("#divisionSelector").val();
    var promiseTeamsData=data.retrieveTeamsData();
    var promisePlayersData=data.retrievePlayersData();
    var promises = [promiseTeamsData, promisePlayersData];
    $.when.apply($, promises).then(function () {
        drawSelectedGraph();
    }, function (e) {
        console.log('No ha sido posible obtener alguno de los datos necesarios para poder dibujar la grafica');
    });
}

//--Utilidades
function getTeamNamesKeyText(){
    return (divisionSelectedValue?divisionSelectedValue:'LigaBBVA')+'TeamNames';
}

function ligaBBVAIsSelected(divisionSelectedValue){
    return (divisionSelectedValue=='LigaBBVA');
}

//--Dibujar graficas
function drawTeamAccumulatedPointsChart() {
    var teams = data.getTeamsData()[getTeamNamesKeyText()];
    charts.drawTeamAccumulatedPointsChart(teams, data.getTeamsData());
}

function drawTeamWeekPointsChart(){
    var teams = data.getTeamsData()[getTeamNamesKeyText()];
    charts.drawTeamWeekPointsChart(teams, data.getTeamsData());
}

function drawTeamWeekGoalsAsVisitorChart() {
    drawTeamWeekGoalsChart(false);
}

function drawTeamWeekGoalsAsLocalChart() {
    drawTeamWeekGoalsChart(true);
}

function drawTeamWeekGoalsChart(isLocak) {
    var teams = data.getTeamsData()[getTeamNamesKeyText()];
    charts.drawTeamWeekGoalsChart(isLocak, teams, data.getTeamsData());
}

function drawPlayerGoalsPercentageChart() {
    charts.drawPlayerGoalsPercentageChart(data.getPlayersData(), ligaBBVAIsSelected(divisionSelectedValue));
}

//--Inicialización
$(document).ready(function () {
    charts.initialize();
    charts.prepareChartButtons();

    data.retrieveTeamsData();
    data.retrievePlayersData();
});