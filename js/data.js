var data = (function () {
    var teamsData = initTeamsData();
    var playersData = initPlayersData();

    var sourceUrls = {
        LigaBBVA: 'http://www.marca.com/futbol/primera/calendario.html',
        LigaAdelante: 'http://www.marca.com/futbol/segunda/calendario.html',
        playersLigaBBVA: 'http://www.20minutos.es/deportes/estadisticas/liga/player_leaders.asp?category=202',
        playersLigaAdelante: 'http://www.20minutos.es/deportes/estadisticas/liga2/player_leaders.asp?category=202'
    };

    function formatPlayerName(name) {
        var namePieces = name.split(',');
        return namePieces[0] + ' (' + namePieces[1].trim() + ')';
    }

    function retrievePlayerNameFromScrappedText(playerData){
        return $(playerData[1]).text().trim();
    }

    function retrievePlayerGoalsFromScrappedText(playerData){
        return $(playerData[4]).text().trim();
    }

    function retrievePlayerShootsOrMatchesFromScrappedText(playerData, divisionSelectedValue){
        if (ligaBBVAIsSelected(divisionSelectedValue)){
            return $(playerData[7]).text().trim();
        } else{
            return $(playerData[2]).text().trim();
        }
    }

    function fillPlayersData(playersData, playerName, shoots, goals, goalShootPrecentage) {
        if (!playersData.fullInformation[playerName]) {
            playersData.fullInformation[playerName] = {};
        }
        playersData.fullInformation[playerName].numeroDisparos = parseInt(shoots);
        playersData.fullInformation[playerName].numeroGoles = parseInt(goals);
        playersData.names.push(playerName);
        playersData.percentages.push(goalShootPrecentage);
    }

    function addPlayerData(playerData, playersData) {
        var playerName = retrievePlayerNameFromScrappedText(playerData);
        //--Some results are just a number so they are dismissed
        if (playerName.indexOf(',') < 0){
            return;
        }
        playerName = formatPlayerName(playerName);
        //TODO: empaquetar estas variables en un objeto JSON para ser creado en un método y luego pasarselo al método fillPlayersData
        var goals = retrievePlayerGoalsFromScrappedText(playerData);
        var shoots = retrievePlayerShootsOrMatchesFromScrappedText(playerData, divisionSelectedValue);
        var goalShootPrecentage = parseFloat((goals / shoots * 100).toFixed(2));

        fillPlayersData(playersData, playerName, shoots, goals, goalShootPrecentage);
    }

    function initTeamsData(){
        return {
            LigaBBVATeamNames: ['Athletic', 'Atletico', 'Barcelona', 'Betis', 'Celta', 'Deportivo', 'Eibar', 'Espanyol', 'Getafe', 'Granada', 'Las Palmas', 'Levante', 'Malaga', 'R. Madrid', 'R. Sociedad', 'Rayo', 'Sevilla', 'Sporting', 'Valencia', 'Villarreal'],
            LigaAdelanteTeamNames: ['Levante','Lugo','Córdoba','Mirandés','Reus','Girona','Real Oviedo','Zaragoza','Sevilla At.','UCAM','Numancia','Elche','Rayo','Huesca','Mallorca','Valladolid','Tenerife','Getafe','Cádiz','Alcorcón','Almería','Gimnástic'],
            graphicTeamNames: ['Huesca', 'Zaragoza', 'Alaves', 'Mallorca'],
            numericData: {
                goalsAsLocal: {},
                goalsAsVisitor: {},
                pointsPerTeam: {},  //It's a hash with the team's name as key and the accumulated points for each week array as value
                pointsPerWeek: {}
            }
        };
    }

    function initPlayersData(){
        return {
            names: [],
            percentages: [],
            fullInformation: {},
            limitNumberPlayers: 30
        };
    }

    function addTeamPoints(localTeamResult, visitorTeamResult, localTeam, visitorTeam) {
        var pointsGot=calculatePointsGotForMatch(localTeamResult, visitorTeamResult);

        assignTeamPoints(teamsData, localTeam, pointsGot.local);
        assignTeamPoints(teamsData, visitorTeam, pointsGot.visitor);

    }

    function assignTeamPoints(teamsData, team, pointsGot){
        var pointsPerTeam=teamsData.numericData.pointsPerTeam;
        var pointsPerWeek=teamsData.numericData.pointsPerWeek;
        //Convertimos los puntos a algo mas visible para ver rachas de vistorias-empates-derrotas. Ejemplo: pointsWithMeaningGot=(pointsGot>1?6:pointsGot==0?2:4)
        var pointsWithMeaningGot = pointsGot;

        if (!pointsPerTeam[team]) {
            pointsPerTeam[team] = [pointsGot];
            pointsPerWeek[team] = [pointsWithMeaningGot];
        } else {
            var teamPoints = pointsPerTeam[team];
            var currentTeamPoints = teamPoints[teamPoints.length - 1];
            teamPoints.push(currentTeamPoints + pointsGot);
            pointsPerWeek[team].push(pointsWithMeaningGot);
        }
    }

    function calculatePointsGotForMatch(localResult, visitorResult){
        var POINTS_FOR_TIE = 1;
        var POINTS_FOR_LOSER = 0;
        var POINTS_FOR_WINNER = 3;

        var pointsGot={};

        localResult = parseInt(localResult);
        visitorResult = parseInt(visitorResult);

        if (localResult < visitorResult) {
            pointsGot.local = POINTS_FOR_LOSER;
            pointsGot.visitor = POINTS_FOR_WINNER;
        } else if (localResult > visitorResult) {
            pointsGot.local = POINTS_FOR_WINNER;
            pointsGot.visitor = POINTS_FOR_LOSER;
        } else{
            pointsGot.local = POINTS_FOR_TIE;
            pointsGot.visitor = POINTS_FOR_TIE;
        }
        return pointsGot;
    }

    var normalizeName = (function () {
        var from = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç",
            to = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc",
            mapping = {};

        for (var i = 0, j = from.length; i < j; i++)
            mapping[from.charAt(i)] = to.charAt(i);

        return function (str) {
            var ret = [];
            for (var i = 0, j = str.length; i < j; i++) {
                var c = str.charAt(i);
                if (mapping.hasOwnProperty(str.charAt(i)))
                    ret.push(mapping[c]);
                else
                    ret.push(c);
            }
            return ret.join('');
        }

    })();

    return {
        getTeamsData:function(){
            return teamsData;
        },
        getPlayersData:function(){
            return playersData;
        },
        retrieveTeamsData: function () {
            var promise = $.get(sourceUrls[divisionSelectedValue], function (res) {
                teamsData = initTeamsData();

                $(res.responseText).find('.final').each(function () {
                    var localResult;
                    var visitorResult;
                    var localTeamName;
                    var visitorTeamName;
                    var wholeResult;
                    if (divisionSelectedValue==='LigaBBVA'){
                        localTeamName = $(this).children('.local')[0].innerHTML;
                    } else{
                        localTeamName = $(this).children('.local')[0].innerHTML;
                    }
                    localTeamName = normalizeName(localTeamName);

                    if (divisionSelectedValue==='LigaBBVA'){
                        visitorTeamName = $(this).children('.visitante')[0].innerHTML;
                    } else{
                        visitorTeamName = $(this).children('.visitante')[0].innerHTML;
                    }
                    visitorTeamName = normalizeName(visitorTeamName);

                    if (divisionSelectedValue==='LigaBBVA'){
                        wholeResult = $(this).children('.resultado')[0].innerHTML;
                    } else{
                        wholeResult = $(this).children('.resultado')[0].innerHTML;
                    }
                    var wholeResultComponents = wholeResult.split('-');
                    localResult = wholeResultComponents[0];
                    visitorResult = wholeResultComponents[1];

                    if (!teamsData.numericData.goalsAsLocal[localTeamName]) {
                        teamsData.numericData.goalsAsLocal[localTeamName] = [];
                    }
                    teamsData.numericData.goalsAsLocal[localTeamName].push(parseInt(localResult));

                    if (!teamsData.numericData.goalsAsVisitor[visitorTeamName]) {
                        teamsData.numericData.goalsAsVisitor[visitorTeamName] = [];
                    }
                    teamsData.numericData.goalsAsVisitor[visitorTeamName].push(parseInt(visitorResult));

                    addTeamPoints(localResult, visitorResult, localTeamName, visitorTeamName);

                });
                $("#graphSelector").show();
            });
            return promise;
        },
        retrievePlayersData: function () {
            playersData = initPlayersData();
            var promise = $.get(sourceUrls['players' + divisionSelectedValue], function (res) {
                $(res.responseText).find('.shsRow0Row, .shsRow1Row').each(function () {
                    var playerData = $(this).children('td');
                    addPlayerData(playerData, playersData);
                });
            });
            return promise;
        },
        getStatistics: function (weeksPoints) {
            var statistics = [0, 0, 0, 0, ""];  //Maximas jornadas seguidas perdiendo, empatando y ganando en ese orden. Ultima racha y tipo
            var lastWeekPoints = -1;
            var losingStreak = 0;
            var tieStreak = 0;
            var winningStreak = 0;
            var streakValue = 0;
            var LOSING_STREAK_TYPE = 1;
            var TIE_STREAK_TYPE = 2;
            var WINNING_STREAK_TYPE = 3;
            var streakType = -1; //1=losing, 2=tie, 3=winning
            for (week in weeksPoints) {
                weekPoints = weeksPoints[week]
                if (lastWeekPoints < 0) {
                    lastWeekPoints = weekPoints;
                } else {
                    if (weekPoints == lastWeekPoints + 1) {
                        if (streakType == TIE_STREAK_TYPE) {
                            streakValue++;
                        } else {
                            streakType = TIE_STREAK_TYPE;
                            streakValue = 1;
                        }
                    }
                    if (weekPoints == lastWeekPoints + 3) {
                        if (streakType == WINNING_STREAK_TYPE) {
                            streakValue++;
                        } else {
                            streakType = WINNING_STREAK_TYPE;
                            streakValue = 1;
                        }
                    }
                    if (weekPoints == lastWeekPoints) {
                        if (streakType == LOSING_STREAK_TYPE) {
                            streakValue++;
                        } else {
                            streakType = LOSING_STREAK_TYPE;
                            streakValue = 1;
                        }
                    }
                }
                if (streakType == LOSING_STREAK_TYPE && statistics[0] < streakValue) {
                    statistics[0] = streakValue;
                }
                if (streakType == TIE_STREAK_TYPE && statistics[1] < streakValue) {
                    statistics[1] = streakValue;
                }
                if (streakType == WINNING_STREAK_TYPE && statistics[2] < streakValue) {
                    statistics[2] = streakValue;
                }
                lastWeekPoints = weekPoints;
            }
            statistics[3] = streakValue;
            statistics[4] = (streakType == 1 ? "Perdidos" : (streakType == TIE_STREAK_TYPE ? "Empatados" : "Ganados"));
            //Mark if last streak has reach its limit
            if (streakType == LOSING_STREAK_TYPE && streakValue == statistics[0]) {
                statistics[5] = '*';
            }
            if (streakType == TIE_STREAK_TYPE && streakValue == statistics[1]) {
                statistics[5] = '*';
            }
            if (streakType == WINNING_STREAK_TYPE && streakValue == statistics[2]) {
                statistics[5] = '*';
            }

            return statistics;
        }
    }
}());