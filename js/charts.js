var charts = (function () {
    var chart;

    //allow highlighting of individual series while muting the others
    function showSeries(e) {
        this.graph.attr("stroke", (e.checked ? this.color : '#ccc'));
        if (e.checked == true) {
            this.group.toFront();
            if (this.visible == false) {
                this.show();
            }
        }
    }
    function highlightSer(chart) {
        var series = chart.series, i;
        for (i = 0; i < series.length; i++) {
            if (series[i].checkbox.checked) {
                showSeries.call(series[i], {checked: true});
            }
        }
    }
    function buildSeriesForChart(teams, teamsData){
        seriesForGraph = [];
        for (indice in teams) {
            var team = teams[indice];
            serieForGraph = {
                name: team,
                data: teamsData[team]
            };
            seriesForGraph.push(serieForGraph);
        }
        return seriesForGraph;
    }

    function drawGenericChart(chartOptions, teams, teamsData){
        var weeks = [];

        var seriesForGraph = buildSeriesForChart(teams, teamsData.numericData[chartOptions.dataFieldForSeries]);

        for (var i = 1; i <= teamsData.numericData.pointsPerTeam[teams[0]].length; i++) {
            weeks.push(i);
        }
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'contenedorGrafico',
                type: chartOptions.type,
                marginRight: 130,
                marginBottom: 25,
                zoomType:"xy"
            },
            title: {
                style: {
                    color: 'blue',
                    fontWeight: 'bold'
                },
                text: chartOptions.titleText,
                x: -20 //center
            },
            subtitle: {
                text: '',
                x: -20
            },
            xAxis: {
                categories: weeks
            },
            yAxis: {
                min: 0,
                title: {
                    text: chartOptions.yAxisTitleText
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: -10,
                y: 100,
                borderWidth: 0
            },
            series: seriesForGraph
        });
        return chart;
    }

    return {
        drawTeamWeekPointsChart: function (teams, teamsData) {
            chartOptions={
                type:'column',
                titleText:'Resultados por jornada',
                subtitleText:'',
                yAxisTitleText:'',
                dataFieldForSeries:'pointsPerWeek'
            };
            return drawGenericChart(chartOptions, teams, teamsData);
        },
        drawTeamAccumulatedPointsChart: function (teams, teamsData) {
            var chartOptions={
                type:'line',
                titleText:'Puntos acumulados hasta la jornada',
                subtitleText:'',
                yAxisTitleText:'Puntos',
                dataFieldForSeries:'pointsPerTeam'
            };
            return drawGenericChart(chartOptions, teams, teamsData);
        },
        drawTeamWeekGoalsChart: function (isLocal, teams, teamsData) {
            var goals = isLocal ? teamsData.numericData.goalsAsLocal : teamsData.numericData.goalsAsVisitor;
            var seriesForGraph = buildSeriesForChart(teams, goals);

            var graphTitleText = 'Goles como ' + (isLocal ? 'LOCAL' : 'VISITANTE') + ' por jornada';
            var graphSubtitleText = '';
            chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'contenedorGrafico',
                    type: 'line',
                    marginRight: 130,
                    marginBottom: 25,
                    zoomType:"xy"
                },
                title: {
                    style: {
                        color: (isLocal ? 'blue' : 'red'),
                        fontWeight: 'bold'
                    },
                    text: graphTitleText,
                    x: -20 //center
                },
                subtitle: {
                    text: graphSubtitleText,
                    x: -20
                },
                xAxis: {
                    min: 1
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Goles'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'top',
                    x: -10,
                    y: 100,
                    borderWidth: 0
                },
                series: seriesForGraph
            });
            return chart;
        },
        drawPlayerGoalsPercentageChart: function(playersData, isLigaBBVA){
            var playerNames = playersData.names.slice(0, playersData.limitNumberPlayers);
            var percentages = playersData.percentages.slice(0, playersData.limitNumberPlayers);
            var chartTitleText=(isLigaBBVA?'Porcentaje efectividad (goles/disparo) de los maximos goleadores actualmente':'Porcentaje efectividad (goles/partidos jugados) de los maximos goleadores actualmente');
            var tooltipTextVariable=(isLigaBBVA?'Goles marcados':'Partidos jugados');
            chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'contenedorGrafico',
                    type: 'column',
                    margin: [50, 50, 200, 80],//[ 50, 50, 100, 80]
                    zoomType:"xy"
                },
                title: {
                    text: chartTitleText
                },
                xAxis: {
                    categories: playerNames,
                    labels: {
                        rotation: -45,
                        align: 'right',
                        style: {
                            fontSize: '11px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Porcentaje efectividad'
                    }
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    formatter: function () {
                        playerName = this.x;
                        return '<b>' + playerName + '</b><br/>' +
                            'Efectividad de un : ' + Highcharts.numberFormat(this.y, 2) +
                            ' % <br/> Disparos realizados: <b>' + playersData.fullInformation[playerName].numeroDisparos + '</b>' +
                            ' <br/> ' + tooltipTextVariable + ': <b>' + playersData.fullInformation[playerName].numeroGoles + '</b>';
                    }
                },
                series: [{
                    name: 'Porcentaje',
                    data: percentages,
                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#FFFFFF',
                        align: 'right',
                        x: 4,
                        y: 10,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                }]
            });
            return chart;
        },
        initialize: function () {
            $("#graphSelector").hide();

            /**
             * Skies theme for Highcharts JS
             * @author Torstein Hønsi
             */

            Highcharts.theme = {
                colors: ["#514F78", "#42A07B", "#9B5E4A", "#72727F", "#1F949A", "#82914E", "#86777F", "#42A07B"],
                chart: {
                    className: 'skies',
                    borderWidth: 0,
                    plotShadow: true,
                    plotBackgroundImage: 'http://www.highcharts.com/demo/gfx/skies.jpg',
                    plotBackgroundColor: {
                        linearGradient: [0, 0, 250, 500],
                        stops: [
                            [0, 'rgba(255, 255, 255, 1)'],
                            [1, 'rgba(255, 255, 255, 0)']
                        ]
                    },
                    plotBorderWidth: 1
                },
                title: {
                    style: {
                        color: '#3E576F',
                        font: '16px Lucida Grande, Lucida Sans Unicode, Verdana, Arial, Helvetica, sans-serif'
                    }
                },
                subtitle: {
                    style: {
                        color: '#6D869F',
                        font: '12px Lucida Grande, Lucida Sans Unicode, Verdana, Arial, Helvetica, sans-serif'
                    }
                },
                xAxis: {
                    gridLineWidth: 0,
                    lineColor: '#C0D0E0',
                    tickColor: '#C0D0E0',
                    labels: {
                        style: {
                            color: '#666',
                            fontWeight: 'bold'
                        }
                    },
                    title: {
                        style: {
                            color: '#666',
                            font: '12px Lucida Grande, Lucida Sans Unicode, Verdana, Arial, Helvetica, sans-serif'
                        }
                    }
                },
                yAxis: {
                    alternateGridColor: 'rgba(255, 255, 255, .5)',
                    lineColor: '#C0D0E0',
                    tickColor: '#C0D0E0',
                    tickWidth: 1,
                    labels: {
                        style: {
                            color: '#666',
                            fontWeight: 'bold'
                        }
                    },
                    title: {
                        style: {
                            color: '#666',
                            font: '12px Lucida Grande, Lucida Sans Unicode, Verdana, Arial, Helvetica, sans-serif'
                        }
                    }
                },
                legend: {
                    itemStyle: {
                        font: '9pt Trebuchet MS, Verdana, sans-serif',
                        color: '#3E576F'
                    },
                    itemHoverStyle: {
                        color: 'black'
                    },
                    itemHiddenStyle: {
                        color: 'silver'
                    }
                },
                labels: {
                    style: {
                        color: '#3E576F'
                    }
                }
            };

            // Apply the theme
            Highcharts.setOptions(Highcharts.theme);
        },
        prepareChartButtons: function () {
            $('#showAll').click(function () {
                for (i = 0; i < chart.series.length; i++) {
                    chart.series[i].show();
                }
            });
            $('#hideAll').click(function () {
                for (i = 0; i < chart.series.length; i++) {
                    chart.series[i].hide();
                }
            });
            $('#checkAll').click(function () {
                for (i = 0; i < chart.series.length; i++) {
                    if (chart.series[i].selected == false) {
                        chart.series[i].select();
                        showSeries.call(chart.series[i], {checked: true});
                    }
                }
            });
            $('#uncheckAll').click(function () {
                for (i = 0; i < chart.series.length; i++) {
                    if (chart.series[i].selected == true) {
                        chart.series[i].select();
                        showSeries.call(chart.series[i], {checked: false});
                    }
                }
            });
    }

    }
}());