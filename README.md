# Sport Charts
Application showing charts for Spanish Soccer matches using highcharts library. 
Data is retrieved from marca.com and 20minutos.com using scraping on client side.
All texts are in Spanish.

# How to use it
Download the project and just open index.html in your browser. There is no need for a server.

Enjoy it!

# Notes
- So far only Liga BBVA and Liga Adelante data is retrieved. No other sports or matches from another countries.
- It uses YQL to make cross-domain requests using a jquery plugin: http://james.padolsey.com/javascript/cross-domain-requests-with-jquery/
- The reason for developing this application was mainly trying to make scrapping from client side so that using a server wasn't required.